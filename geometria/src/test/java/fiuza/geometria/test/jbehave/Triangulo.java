package fiuza.geometria.test.jbehave;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Triangulo {
	public static WebDriver driver;

	@Test
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:/Users/yla_m/Downloads/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("C:/Users/yla_m/git/geometria-2020-1/geometria/src/main/webapp/triangulo.html");
	}
	
	@Test
	public void selecionarHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");

	}
	
	@Test
	public void selecionarCateto() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");

	}

	@Test
	public void informarCatetoOposto(String cateto1) {
		WebElement CatetoOposto = driver.findElement(By.id("cateto1"));
		CatetoOposto.sendKeys(cateto1);


	}

	@Test
	public void informarCatetoAdjacente(String cateto2)  {
		WebElement CatetoAdjacente = driver.findElement(By.id("cateto2"));	
		CatetoAdjacente.sendKeys(cateto2);

	}
	
	@Test
	public void informarHipotenusa(String hipotenusa) {
		WebElement Hipotenusa = driver.findElement(By.id("hipotenusa"));
		Hipotenusa.sendKeys(hipotenusa);
	}

	
	
	@Test
	public String obterHipotenusa() {
		
		WebElement valor = driver.findElement(By.id("hipotenusa"));
		return valor.getText();
	}

	@Test
	public String obterCatetos() {
		WebElement valor = driver.findElement(By.id("cateto2"));
		return valor.getText();
	}
	
	@Test
	public void calcular() {
		WebElement calcularBtn = driver.findElement(By.id("calcularBtn"));
		calcularBtn.click();
	}
	
	@AfterAll
	public static void off() {
		driver.quit();
	}
}
